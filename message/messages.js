const express = require('express');
const router = express.Router();
const Vigenere = require('caesar-salad').Vigenere;

router.post('/encode', (req, res) => {
    const message = Vigenere.Cipher(req.body.password).crypt(req.body.message);
    res.send(message);
});

router.post('/decode', (req, res) => {
    console.log(req.body);
    const message = Vigenere.Decipher(req.body.password).crypt(req.body.message);
    res.send(message);
});

module.exports = router;