const express = require ('express');
const messages = require('./message/messages');
const cors = require('cors');

const app = express();
app.use(express.json());
app.use(cors());
const port = 8001;

app.use('/message', messages);

app.listen(port,() =>{
    console.log('We are live on http://localhost: '+ port);
});
